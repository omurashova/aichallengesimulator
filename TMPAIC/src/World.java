import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



public class World {
	
	public static Map mapOriginal;
	public static Map mapWork;
	private int idAnt=0;
	private JSONObject obj;
	private FileWriter file;
	public static ArrayList<Anthill> listAnthill = new ArrayList<Anthill>(); 
	public static ArrayList<Ant> AllAnt = new ArrayList<Ant>(); 
	public int CountOfPlayer = 0;
	public int win;

	
	public  World(){
		try{	
			this.mapOriginal = new Map();
			this.mapWork= new Map();
			obj = new JSONObject();
			file = new FileWriter("./src/log/log.json");
			//��������� ������ ������������, � � ��� �������� �������� 
		}
		catch(IOException e){ System.out.print("Exception "); }
	
	}
	public void WordSimulation(){
		for(int count=0;count<10;count++){
	
			for(int i=1;i<=2;i++){
				try{
					SwitchPlayer(i);
				}catch (Exception e){PushMatrixJsonFileLog("���� ����������� ������");}
			}
			if(listAnthill.size()==1) {//����������
				PushMatrixJsonFileLog(this.mapWork.matrix,listAnthill.get(0).getId() );
				String[] lines =listAnthill.get(0).getId().split ("_");
				win= Integer.parseInt(lines[1]);
				//System.out.println("!!!!!!!!Win   "+win); 
					return;
				}
			if(listAnthill.size()==0) {//�����
				PushMatrixJsonFileLog(this.mapWork.matrix,"draw");
				win= -1;
				//System.out.println("!!!!!!!!Win   "+win); 
					return;
				}
		PushMatrixJsonFileLog(this.mapWork.matrix,"");
		}
		PushMatrixJsonFileLog(this.mapWork.matrix,"not finished");
		try{
		file.close();}catch (IOException e) {
			e.printStackTrace();}
	}
	
	public boolean GameEnd(){return true;}
	
	public void RandomAnt(){ 

		for(int i=0;i<mapWork.size;i++){
			for(int j=0;j<mapWork.size;j++){ 
				if(mapWork.matrix[i][j].equals("anthill_1")){ listAnthill.add(new Anthill("anthill_1",i,j));
					this.mapWork.matrix[i+2][j+2]="A"+"1"+GenIdAnt();
					SwitchPlayerAddAnt("1",this.mapWork.matrix[i+2][j+2],i+2,j+2,"anthill_1");
					this.mapWork.matrix[i+1][j+1]="A"+"1"+GenIdAnt();
					SwitchPlayerAddAnt("1",this.mapWork.matrix[i+1][j+1],i+1,j+1,"anthill_1");
				     //break;
					}
				if(mapWork.matrix[i][j].equals("anthill_2")){ listAnthill.add(new Anthill("anthill_2",i,j));
					this.mapWork.matrix[i-2][j-2]="A"+"2"+GenIdAnt();
					SwitchPlayerAddAnt("2",this.mapWork.matrix[i-2][j-2],i-2,j-2,"anthill_2");
					this.mapWork.matrix[i-1][j-1]="A"+"2"+GenIdAnt();
					SwitchPlayerAddAnt("2",this.mapWork.matrix[i-1][j-1],i-1,j-1,"anthill_2");
				    // break;
					}
			}
			}
		
			
	}
	
	public void SwitchPlayerAddAnt(String num, String id,int x,int y, String findHill){
		AllAnt.add(new Ant(id,x,y,findHill));
		switch(num){
		case "1":{   Main.firstPlayer.listAnt.add(new Ant(id,x,y,findHill));  break; }
		case "2": {  Main.secondPlayer.listAnt.add(new Ant(id,x,y,findHill));  break; }
//		case 3:{ }
//		case 4:{ }
//		case 5:{ }
//		case 6:{ }
//		case 7:{ }
		
	}
	}
	public void SwitchPlayer(int num){
		switch(num){
		case 1:{  Main.firstPlayer.Function(); break;  }
		case 2:{ Main.secondPlayer.Function(); break;}
		case 3:{ }
		case 4:{ }
		case 5:{ }
		case 6:{ }
		case 7:{ }
		
	}
	}
	private String GenIdAnt(){ return Integer.toString(this.idAnt++); }
	
//	private String FinalLose(){
	//	for(int i=0;i<this.mapWork.matrix.length;i++){
		//	for(int j=0;j<this.mapWork.matrix.length;j++){ 
			//}
		//	}
	//	return ;
//	}
	
	
	public void PushMatrixJsonFileLog(String[][] map, String finalwin){
		
		JSONArray list = new JSONArray();
		
		for(int i=0;i<map.length;i++){
			for(int j=0;j<map.length;j++){ 
				list.add(map[i][j]);
			}
		}
		obj.put("map", list);
		if(finalwin.length()!=0){obj.put("winner", finalwin); obj.put("Info", "Congratulations");}
		try {
			
			file.write(obj.toString()+",");
			file.flush();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
public void PushMatrixJsonFileLog(String error){
		
		JSONArray list = new JSONArray();
				list.add(error);
		obj.put("error", list);
		try {
			
			file.write(obj.toString());
			file.flush();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
